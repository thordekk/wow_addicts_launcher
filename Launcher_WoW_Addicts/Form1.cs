﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.AccessControl;
using System.Security;
using System.Security.Permissions;

namespace Launcher_WoW_Addicts
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string browserpath_legion = Application.StartupPath + "/Client_path.txt";
            string Client_path = File.ReadLines(browserpath_legion).First();

            System.Diagnostics.Process.Start(Client_path + "/NaturalStorm_64.exe");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string setpath = "";

            FolderBrowserDialog browse = new FolderBrowserDialog();
            browse.Description = "Choose your instalation folder: ";
            if (browse.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                setpath = browse.SelectedPath;

                string savepathfile = Application.StartupPath + "/Client_path.txt";
                File.WriteAllText(savepathfile, setpath);
                MessageBox.Show("The client path is saved correctly");
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.naturalstorm-wowservers.com/");
        }
    }
}
